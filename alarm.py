import datetime
import time
import pygame
ars = 0
alarm = 0
pygame.init()
pygame.mixer.init()
pygame.mixer.music.load("music.mp3")
import json

class Button:
    def __init__(self, x, y, width, height, text):
        self.rect = pygame.Rect(x, y, width, height)
        self.text = text
        self.clicked = False
        self.normal_color = (200, 200, 200)
        self.hover_color = (150, 150, 150)
        self.selected_color = (100, 100, 100)
        self.font = pygame.font.SysFont('Arial', 100)
        self.surface = self.font.render(self.text, True, (0, 0, 0))

    def draw(self, surface):
        color = self.normal_color

        if self.rect.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                color = self.selected_color
            else:
                color = self.hover_color

        pygame.draw.rect(surface, color, self.rect)
        text_rect = self.surface.get_rect(center=self.rect.center)
        surface.blit(self.surface, text_rect)

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                if self.rect.collidepoint(event.pos):
                    self.selected = True
        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                if self.rect.collidepoint(event.pos) and self.selected:
                    self.selected = False
                    self.clicked = True
                else:
                    self.selected = False

    def is_clicked(self):
        if self.clicked:
            self.clicked = False
            return True
        return False
    



dictionary = {
    "todo": '[7.40,"be sure to pack your bag (and take extra food)"]',
    "alarm":7.00,
    "alarmschedule":'[0,1,2,3,4]',
    "alarmon":1,
    "loving olivia,le":1
}
alarmschedule = [0,1,2,3,4]
alarmon = 1
alarmtime = 7.00
def writedt(todo,alarm,alarmON,alarmschedule):
    global dictionary
    t = dictionary
    t["todo"] = todo
    t["alarm"] = alarm
    t["alarmon"] = alarmON
    t["alarmschedule"] = alarmschedule
    with open("/run/user/1000/alarmsettings.json", "w") as outfile:
        json.dump(dictionary, outfile)
    
def readdt():
    global dictionary,alarmschedule,alarmon,alarmtime
    try:
        with open('/run/user/1000/alarmsettings.json', 'r') as openfile:
            dti = json.load(openfile)
            alarmschedule = eval(dti["alarmschedule"])
            alarmtime = dti["alarm"]
            alarmon = dti["alarmon"]
    except:
        writedt(dictionary["todo"],dictionary["alarm"],dictionary["alarmon"],dictionary["alarmschedule"])
        readdt()
def getminute(i):
    return int((i * 100 ) - (int(i) * 100))
def gethour(i):
    return int(i)
def check():
    global alarmtime,alarmon,alarmschedule
    t = datetime.datetime.now()
    t.minute
    aminute = getminute(alarmtime)
    set_off = 0
    if int(t.weekday()) in alarmschedule:
        if t.minute == aminute :
            if t.hour == gethour(alarmtime):
                set_off = 1
    if alarmon == 0:
        set_off = 0
    return set_off

stopbtn = Button(0,0,800,600,"stop  alarm")
screen = pygame.display.set_mode((800, 600), flags=pygame.HIDDEN)
frames = 0
while True:
    pygame.display.flip()
    time.sleep(0.1)
    frames = (frames + 1) % 1200
    if frames == 2:
        readdt()
    for event in pygame.event.get():
            stopbtn.handle_event(event)
            if stopbtn.is_clicked():
                alarm = False
                pygame.mixer.music.fadeout(100)
                screen = pygame.display.set_mode((800, 600), flags=pygame.HIDDEN)
            stopbtn.draw(screen)
            pygame.display.flip()

    if  ars ==0 and check():
        alarm = True
        pygame.mixer.music.play(10)
        screen = pygame.display.set_mode((800, 600), flags=pygame.SHOWN)
        ars = 1
        
        
        
    else:
        if not check():
            ars = 0
        alarm = False
        #screen = pygame.display.set_mode((800, 600), flags=pygame.HIDDEN)
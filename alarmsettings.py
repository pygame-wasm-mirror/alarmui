import json
import datetime


dictionary = {
    "todo": '[7.40,"be sure to pack your bag (and take extra food)"]',
    "alarm":7.00,
    "alarmschedule":'[0,1,2,3,4]',
    "alarmon":1,
    "loving olivia,le":1
}
alarmschedule = [0,1,2,3,4]
alarmon = 1
alarmtime = 7.00
def writedt(todo,alarm,alarmON,alarmschedule):
    global dictionary
    t = dictionary
    t["todo"] = todo
    t["alarm"] = alarm
    t["alarmon"] = alarmON
    t["alarmschedule"] = alarmschedule
    with open("/run/user/1000/alarmsettings.json", "w") as outfile:
        json.dump(dictionary, outfile)
    
def readdt():
    global dictionary,alarmschedule,alarmon,alarmtime
    try:
        with open('/run/user/1000/alarmsettings.json', 'r') as openfile:
            dti = json.load(openfile)
            alarmschedule = eval(dti["alarmschedule"])
            alarmtime = dti["alarm"]
            alarmon = dti["alarmon"]
    except:
        writedt(dictionary["todo"],dictionary["alarm"],dictionary["alarmon"],dictionary["alarmschedule"])
        readdt()
    # Reading from json file
    
def getminute(i):
    return int((i * 100 ) - (int(i) * 100))
def gethour(i):
    return int(i)
def check():
    global alarmtime,alarmon,alarmschedule
    t = datetime.datetime.now()
    t.minute
    aminute = getminute(alarmtime)
    set_off = 0
    if int(t.weekday()) in alarmschedule:
        if t.minute == aminute :
            if t.hour == gethour(alarmtime):
                set_off = 1
    if alarmon == 0:
        set_off = 0
    return set_off

    